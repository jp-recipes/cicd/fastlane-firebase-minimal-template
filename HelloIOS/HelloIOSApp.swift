//
//  HelloIOSApp.swift
//  HelloIOS
//
//  Created by jp on 06-06-23.
//

import SwiftUI

@main
struct HelloIOSApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
