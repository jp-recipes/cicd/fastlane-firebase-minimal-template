//
//  ContentView.swift
//  HelloIOS
//
//  Created by jp on 06-06-23.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack {
            Text("Hello, world! - Firebase - Web")
        }
        .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
